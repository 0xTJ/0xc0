#include "0xc0.h"
#include <stdio.h>

int myfunc(c0_r_state_arg) {
    c0_r_state_begin;
    int myvar1;
    c0_r_state_end(state);

    c0_r_begin(state);

    state->myvar1 = 0;

    printf("1\n");
    c0_r_yield(3);
    c0_r_stop(4);
    printf("2\n");

    c0_r_end(0);
}

void myfunc3(c0_r_state_arg) {
    c0_r_state_begin;
    int myvar1;
    c0_r_state_end(state);

    c0_r_begin(state);

    state->myvar1 = 0;

    c0_r_abort();
    c0_r_stop();
    c0_r_yield();

    printf("1\n");
    c0_r_yield();
    printf("2\n");

    c0_r_end();
    return (void)0;
}

void myfunc2() {
    if (0) c0_begin();

    if (0) {c0_end();} while (0);
}

int main() {
    c0_r_state state = C0_R_STATE_NULL;
    c0_r_state state2 = C0_R_STATE_NULL;
    printf("bet %d\n", myfunc(&state));
    printf("bat %d\n", myfunc(&state2));
    printf("bet %d\n", myfunc(&state));
    printf("bet %d\n", myfunc(&state));
    printf("bat %d\n", myfunc(&state2));
    printf("bat %d\n", myfunc(&state2));
}
