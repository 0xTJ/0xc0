#ifndef INCLUDE_0XC0_H
#define INCLUDE_0XC0_H

// The c0_x macros are for static non-reentrant functions.
// Use c0_r_x for re-entrant functions.

// The coroutine code must be between c0[_r]_begin() and c0[_r]_end().
// Use c0[_r]_yield() and c0[_r]_stop() to return from the function.
// Yielding will cause the function to continue from the point after the yield.
// Stopping will clear the line state and the next call will run from begin.
// The macros c0[_r]_abort clear the line state, but continue execution.
// End, yield, and stop must be passed return values in non-void functions.

// Re-entrant functions require c0_r_state_arg to be present in the parameters.
// A pointer to a variable of type c0_r_state must be passed on each call.
// This variable contains the state of the coroutine function.

// Re-entrants functions require a state type to be created.
// Members must be declared between c0_r_state_begin and c0_r_state_end().
// An object name must be passed to c0_r_state_end().
// This becomes the name of a pointer to the state.

// Defining C0_DISABLE_REENTRANT will disable re-entrant function support.

#ifndef C0_DISABLE_REENTRANT
#include <stdlib.h>
#endif

#define c0_begin()                                  \
    do {                                            \
        static unsigned long _c0_saved_line = 0;    \
        switch (_c0_saved_line) {                   \
        case 0: do {} while (0)

#define c0_end(_c0_ret_val) \
        }                   \
        _c0_saved_line = 0; \
        return _c0_ret_val; \
    } while (0)

#define c0_yield(_c0_ret_val)       \
    do {                            \
        _c0_saved_line = __LINE__;  \
        return _c0_ret_val;         \
        case __LINE__:;             \
    } while (0)

#define c0_stop(_c0_ret_val)    \
    do {                        \
        _c0_saved_line = 0;     \
        return _c0_ret_val;     \
    } while (0)

#define c0_abort()          \
    do {                    \
        _c0_saved_line = 0; \
    } while (0)


#ifndef C0_DISABLE_REENTRANT

typedef void *c0_r_state;
#define C0_R_STATE_NULL NULL
#define c0_r_state_arg c0_r_state *_c0_state_arg

#define c0_r_state_begin                \
    struct _c0_state_struct {           \
        unsigned long _c0_saved_line
    
#define c0_r_state_end(_c0_state)   \
    } *_c0_state = *_c0_state_arg;  \
    do {} while (0)

#define c0_r_begin(_c0_state)                                   \
    do {                                                        \
        if (!(_c0_state)) {                                     \
            (_c0_state) = malloc(sizeof(*_c0_state));           \
            *_c0_state_arg = (_c0_state);                       \
            if (_c0_state) {                                    \
                (_c0_state)->_c0_saved_line = 0;                \
            }                                                   \
        }                                                       \
        if (_c0_state) switch ((_c0_state)->_c0_saved_line) {   \
        case 0: do {} while (0)

#define c0_r_end(_c0_ret_val)    \
        }                        \
        *_c0_state_arg = NULL;   \
        free(*_c0_state_arg);    \
        return _c0_ret_val;      \
    } while (0)

#define c0_r_yield(_c0_ret_val)                             \
    do {                                                    \
        struct _c0_state_struct *_c0_tmp = *_c0_state_arg;  \
        _c0_tmp->_c0_saved_line = __LINE__;                 \
        return _c0_ret_val;                                 \
        case __LINE__:;                                     \
    } while (0)

#define c0_r_stop(_c0_ret_val)  \
    do {                        \
        *_c0_state_arg = NULL;  \
        free(*_c0_state_arg);   \
        return _c0_ret_val;     \
    } while (0)

#define c0_r_abort()            \
    do {                        \
        *_c0_state_arg = NULL;  \
        free(*_c0_state_arg);   \
    } while (0)

#endif

#endif
